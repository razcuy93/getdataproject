from _json import make_scanner

import PyPDF2
import xlwt
from xlwt import Workbook
import re
import xlsxwriter
from api_get_data.models import LiabilityTemplate,PackagTemplate,GarageTemplate,PropertyTemplate, File
import datetime

import pdfquery
from lxml import etree

def Liability_Template(url):
    conter=0
#    Liability=LiabilityTemplate()
    pdfFileObj = open(url, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    ct=0
    value = 0
    pos = -1
    bpremium = False
    premieum = ''
    policeFee = ''
    inspectionFe = ''
    surpluse = ''
    fee = ''
    tax = ''
    fhcl = ''

    for x in range(0, pdfReader.numPages):
        pageObj = pdfReader.getPage(x)
        text = pageObj.extractText()
        pos = text.find('Liability Premium')
        if(pos!= -1):
            pos = pos + 93
            premieum = text[pos:pos + 10]
            characters = "'!?*:,;- "
            premieum = ''.join(x for x in premieum if x not in characters)
            premieum=premieum.strip()
            premieum=checkString(premieum)
            conter=conter+1
        pos = text.find('Premium:')
        if (pos != -1):
            pos = pos + 102
            
            premieum = text[pos:pos + 10]
            characters = "'!?*:,;- "
            premieum = ''.join(x for x in premieum if x not in characters)
            premieum=premieum.strip()
            premieum=checkString(premieum)
            conter=conter+1

            print("Premium:", premieum)
        pos = text.find('Policy Fee')
        if (pos != -1):
            pos = pos + 10
            policeFee = text[pos:pos + 8]
            characters = "'!?*:,;- "
            policeFee = ''.join(x for x in policeFee if x not in characters)
            policeFee=policeFee.strip()
            print("Policy Fee :", policeFee)
            conter = conter + 1
        pos = text.find('Inspection Fee')
        if (pos != -1):
            pos = pos + 14
            inspectionFe = text[pos:pos + 8]
            characters = "'!?*:,;- "
            inspectionFe = ''.join(x for x in inspectionFe if x not in characters)
            inspectionFe = inspectionFe.strip()
            print("Inspection Fee :", inspectionFe)
            conter = conter + 1
        pos = text.find('Surplus Tx')
        if (pos != -1):
            pos = pos + 12
            surpluse = text[pos:pos + 8]
            characters = "'!?*:,;- "
            surpluse = ''.join(x for x in surpluse if x not in characters)
            surpluse = surpluse.strip()
            conter = conter + 1
            print("Surplus Tx:", surpluse)

        pos = text.find('Surplus Lines Tax')
        if (pos != -1):
            pos = pos + 17
            surpluse = text[pos:pos + 8]
            characters = "'!?*:,;- "
            surpluse = ''.join(x for x in surpluse if x not in characters)
            surpluse = surpluse.strip()
            conter = conter + 1
            print("Surplus Lines Tax:", surpluse)
        pos = text.find('Service Off Fee')
        if (pos != -1):
            pos = pos + 14
            fee = text[pos:pos + 7]
            characters = "'!?*:,;- "
            fee = ''.join(x for x in fee if x not in characters)
            fee = fee.strip()
            conter = conter + 1
            print("Service Off Fee:", fee)
        pos = text.find('Service Fee')
        if (pos != -1):
            pos = pos + 11
            fee = text[pos:pos + 7]
            characters = "'!?*:,;- "
            fee = ''.join(x for x in fee if x not in characters)
            fee = fee.strip()
            conter = conter + 1
            print("Service Fee:", fee)
        pos = text.find('Tax State:')
        if (pos != -1 and ct==0):
            pos = pos + 10
            tax = text[pos:pos + 4]
            characters = "'!?*:,;- "
            tax = ''.join(x for x in tax if x not in characters)
            tax = tax.strip()
            ct=1
            conter = conter + 1
            print("Tax State:", tax)
        pos = text.find('FHCL')
        if (pos !=-1):
            pos=pos+4
            fhcl= text[pos:pos + 6]
            characters = "'!?*:,;- "
            tax = ''.join(x for x in fhcl if x not in characters)
            conter = conter + 1
            tax = tax.strip()


    Liability= LiabilityTemplate(liability_premium=premieum,policy_fee= policeFee, inspection_fee=inspectionFe, surplus=surpluse, service_off=fee ,fhcl=fhcl, fl=tax)
    Liability.save()
    pross='unprocessed file'
    if conter>0:
        pross = 'successfully processed'
   
    date=datetime.datetime.now()
    data_string = date.strftime('%d/%m/%Y %H:%M:%S')

    file=File(file_name=url, classification='Leabiliti', status=pross, datetime=data_string)
    file.save()

def checkString(str): 
             
    for e in str:
        if e.isalpha():
            str=str.replace(e,"")

  
    return str        


def property_templeta(url):
    pdfFileObj = open(url, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    conter=0
    ct=0
    value = 0
    pos = -1
    bpremium = False
    premieum = ''
    policeFee = ''
    inspectionFe = ''
    surpluse = ''
    fee = ''
    tax = ''
    provider = ''

    for x in range(0, pdfReader.numPages):
        pageObj = pdfReader.getPage(x)
        text = pageObj.extractText()
        pos = text.find('Premium:')
        if (pos != -1):
            pos = pos + 102
            premieum = text[pos:pos + 10]
            characters = "'!?*:,;- "
            premieum = ''.join(x for x in premieum if x not in characters)
            premieum=premieum.strip()
            conter = conter+1

            print("Premium:", premieum)
        pos = text.find('Policy Fee')
        if (pos != -1):
            pos = pos + 10
            policeFee = text[pos:pos + 8]
            characters = "'!?*:,;- "
            policeFee = ''.join(x for x in policeFee if x not in characters)
            policeFee=policeFee.strip()
            print("Policy Fee :", policeFee)
            conter = conter+1
        pos = text.find('Inspection Fee')
        if (pos != -1):
            pos = pos + 14
            inspectionFe = text[pos:pos + 8]
            characters = "'!?*:,;- "
            inspectionFe = ''.join(x for x in inspectionFe if x not in characters)
            inspectionFe = inspectionFe.strip()
            print("Inspection Fee :", inspectionFe)
            conter = conter + 1
        pos = text.find('Surplus Tx')
        if (pos != -1):
            pos = pos + 12
            surpluse = text[pos:pos + 8]
            characters = "'!?*:,;- "
            surpluse = ''.join(x for x in surpluse if x not in characters)
            surpluse = surpluse.strip()
            print("Surplus Tx:", surpluse)
            conter = conter + 1

        pos = text.find('Surplus Lines Tax')
        if (pos != -1):
            pos = pos + 17
            surpluse = text[pos:pos + 8]
            characters = "'!?*:,;- "
            surpluse = ''.join(x for x in surpluse if x not in characters)
            surpluse = surpluse.strip()
            print("Surplus Lines Tax:", surpluse)
            conter = conter + 1
        pos = text.find('Stamping Office Fee')
        if (pos != -1):
            pos = pos + 18
            fee = text[pos:pos + 8]
            characters = "'!?*:,;- "
            fee = ''.join(x for x in fee if x not in characters)
            fee = fee.strip()
            print("Stamping Office Fee", fee)
            conter = conter + 1
        pos = text.find('Stamping Fee')
        if (pos != -1):
            pos = pos + 11
            fee = text[pos:pos + 7]
            characters = "'!?*:,;- "
            fee = ''.join(x for x in fee if x not in characters)
            fee = fee.strip()
            conter = conter + 1
            print("Service Fee:", fee)
        pos = text.find('FL EMPATF')
        if (pos != -1 and ct==0):
            pos = pos + 10
            tax = text[pos:pos + 4]
            characters = "'!?*:,;- "
            tax = ''.join(x for x in tax if x not in characters)
            tax = tax.strip()
            ct=1
            conter = conter + 1
            print("Tax State:", tax)
        pos = text.find('Provider Fee')
        if (pos !=-1):
            pos=pos+11
            provider= text[pos:pos + 6]
            characters = "'!?*:,;- |"
            provider = ''.join(x for x in provider if x not in characters)
            provider = tax.strip()
            conter = conter + 1
    property=PropertyTemplate(premium_prop='premieum', police_fe=policeFee, inspection_fe=inspectionFe, surplus=surpluse, stamping=fee, fl_empatf=tax, provider=provider)
    property.save()
    pross='unprocessed file'
    if conter>0:
        pross = 'successfully processed'
    
    date=datetime.datetime.now()
    data_string = date.strftime('%d/%m/%Y %H:%M:%S')

    file=File(file_name=url, classification='Property', status=pross, datetime=data_string)
    file.save()

def garage_template(url):
   # - Premium
   # - Policy Fee
   # - Inspection Fee
   # - Surplus
   # Tx / Surplus Lines Tax
    pdfFileObj = open(url, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    ct = 0
    value = 0
    pos = -1
    premium = ''
    policeFee = ''
    inspectionFe = ''
    surpluse = ''
    conter=0
    for x in range(0, pdfReader.numPages):
        pageObj = pdfReader.getPage(x)
        text = pageObj.extractText()
        pos = text.find('Premium:')
        if (pos != -1):
            pos = pos + 102
            premieum = text[pos:pos + 10]
            characters = "'!?*:,;- "
            premieum = ''.join(x for x in premieum if x not in characters)
            premieum = premieum.strip()
            conter=conter+1

        print("Premium:", premieum)
        pos = text.find('Policy Fee')
        if (pos != -1):
            pos = pos + 10
            policeFee = text[pos:pos + 8]
            characters = "'!?*:,;- "
            policeFee = ''.join(x for x in policeFee if x not in characters)
            policeFee = policeFee.strip()
            print("Policy Fee :", policeFee)
            conter=conter+1
        pos = text.find('Inspection Fee')
        if (pos != -1):
            pos = pos + 14
            inspectionFe = text[pos:pos + 8]
            characters = "'!?*:,;- "
            inspectionFe = ''.join(x for x in inspectionFe if x not in characters)
            inspectionFe = inspectionFe.strip()
            print("Inspection Fee :", inspectionFe)
            conter=conter+1
        pos = text.find('Surplus Tx')
        if (pos != -1):
            pos = pos + 12
            surpluse = text[pos:pos + 8]
            characters = "'!?*:,;- "
            surpluse = ''.join(x for x in surpluse if x not in characters)
            surpluse = surpluse.strip()
            conter=conter+1
            print("Surplus Tx:", surpluse)

        pos = text.find('Surplus Lines Tax')
        if (pos != -1):
            pos = pos + 17
            surpluse = text[pos:pos + 8]
            characters = "'!?*:,;- "
            surpluse = ''.join(x for x in surpluse if x not in characters)
            surpluse = surpluse.strip()
            conter=conter+1
            print("Surplus Lines Tax:", surpluse)
    garage=GarageTemplate(premium_gar=premieum,police_fe=policeFee, inspection_fe=inspectionFe, surplus=surpluse)
    garage.save()
    pross = 'unprocessed file'
    if conter > 0:
        pross = 'successfully processed'
    date=datetime.datetime.now()
    data_string = date.strftime('%d/%m/%Y %H:%M:%S')

    file=File(file_name=url, classification='Garage', status=pross, datetime=data_string)
    file.save()

def Package_Template(url):
    conter=0
    pdfFileObj = open(url, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    ct=0
    value = 0
    pos = -1
    bpremium = False
    premieum = ''
    policeFee = ''
    inspectionFe = ''
    surpluse = ''
    fee = ''
    misc = ''
    tax = ''
    fhcl = ''

    for x in range(0, pdfReader.numPages):
        pageObj = pdfReader.getPage(x)
        text = pageObj.extractText()
        pos = text.find('Package Premium')
        if(pos!= -1):
            pos = pos + 93
            premieum = text[pos:pos + 10]
            characters = "'!?*:,;- "
            premieum = ''.join(x for x in premieum if x not in characters)
            premieum=premieum.strip()
            conter=conter+1
        pos = text.find('Premium:')
        if (pos != -1):
            pos = pos + 102
            premieum = text[pos:pos + 10]
            characters = "'!?*:,;- "
            premieum = ''.join(x for x in premieum if x not in characters)
            premieum=premieum.strip()
            conter = conter + 1

            print("Premium:", premieum)
        pos = text.find('Policy Fee')
        if (pos != -1):
            pos = pos + 10
            policeFee = text[pos:pos + 8]
            characters = "'!?*:,;- "
            policeFee = ''.join(x for x in policeFee if x not in characters)
            policeFee=policeFee.strip()
            conter = conter + 1
            print("Policy Fee :", policeFee)
        pos = text.find('Inspection Fee')
        if (pos != -1):
            pos = pos + 14
            inspectionFe = text[pos:pos + 8]
            characters = "'!?*:,;- "
            inspectionFe = ''.join(x for x in inspectionFe if x not in characters)
            inspectionFe = inspectionFe.strip()
            conter = conter + 1
            print("Inspection Fee :", inspectionFe)
        pos = text.find('Carrier Insp Fee')
        if (pos != -1):
            pos = pos + 14
            inspectionFe = text[pos:pos + 8]
            characters = "'!?*:,;- "
            inspectionFe = ''.join(x for x in inspectionFe if x not in characters)
            inspectionFe = inspectionFe.strip()
            conter = conter + 1
            print("Carrier Insp Fee :", inspectionFe)
        pos = text.find('Surplus Tx')
        if (pos != -1):
            pos = pos + 12
            surpluse = text[pos:pos + 8]
            characters = "'!?*:,;- "
            surpluse = ''.join(x for x in surpluse if x not in characters)
            surpluse = surpluse.strip()
            conter = conter + 1
            print("Surplus Tx:", surpluse)

        pos = text.find('Surplus Lines Tax')
        if (pos != -1):
            pos = pos + 17
            surpluse = text[pos:pos + 8]
            characters = "'!?*:,;- "
            surpluse = ''.join(x for x in surpluse if x not in characters)
            surpluse = surpluse.strip()
            print("Surplus Lines Tax:", surpluse)
            conter = conter + 1
        pos = text.find('Service Office Fee')
        if (pos != -1):
            pos = pos + 14
            fee = text[pos:pos + 7]
            characters = "'!?*:,;- "
            fee = ''.join(x for x in fee if x not in characters)
            fee = fee.strip()
            print("Service Off Fee:", fee)
            conter = conter + 1
        pos = text.find('Service Fee')
        if (pos != -1):
            pos = pos + 11
            fee = text[pos:pos + 7]
            characters = "'!?*:,;- "
            fee = ''.join(x for x in fee if x not in characters)
            fee = fee.strip()
            print("Service Fee:", fee)
            conter = conter + 1
        #Misc State Tax
        pos = text.find('Misc State Tax')
        if (pos != -1):
            pos = pos + 15
            misc = text[pos:pos + 7]
            characters = "'!?*:,;- "
            misc = ''.join(x for x in misc if x not in characters)
            misc = misc.strip()
            print("Misc:", misc)
            conter = conter + 1

    date=datetime.datetime.now()
    data_string = date.strftime('%d/%m/%Y %H:%M:%S')


    pross = 'unprocessed file'
    if conter > 0:
        pross = 'successfully processed'
    file = File(file_name=url, classification='Package', status=pross, datetime=data_string)
    file.save()


#Liability_Template("D:\\Descargas\\Descargas 2022\\Telegram Desktop\\Pages_from_RWL_Renewal_Quote_to_Agent_4_19_2022_GENERAL_LIABILITY.pdf")
#garage_template("D:\\Descargas\\Descargas 2022\\Telegram Desktop\\Pages_from_RWL_Renewal_Quote_to_Agent_4_19_2022_GENERAL_LIABILITY.pdf")
#Package_Template("D:\\Descargas\\Descargas 2022\\Telegram Desktop\\Pages_from_RWL_Renewal_Quote_to_Agent_4_19_2022_GENERAL_LIABILITY.pdf")