from django.db import models
from model_utils.models import TimeStampedModel, SoftDeletableModel

# Create your models here.

class LiabilityTemplate(models.Model):
    liability_premium=models.TextField(max_length=5000, null=False, blank=True)
    policy_fee=models.TextField(max_length=5000, null=False, blank=True)
    inspection_fee=models.TextField(max_length=5000, null=False, blank=True)
    surplus=models.TextField(max_length=5000, null=False, blank=True)
    service_off=models.TextField(max_length=5000, null=False, blank=True)
    fhcl=models.TextField(max_length=5000, null=False, blank=True)
    fl=models.TextField(max_length=5000, null=False, blank=True)

class PropertyTemplate(models.Model):
    premium_prop=models.TextField(max_length=5000, null=False, blank=True)
    police_fe=models.TextField(max_length=5000, null=False, blank=True)
    inspection_fe=models.TextField(max_length=5000, null=False, blank=True)
    surplus=models.TextField(max_length=5000, null=False, blank=True)
    stamping=models.TextField(max_length=5000, null=False, blank=True)
    fl_empatf=models.TextField(max_length=5000, null=False, blank=True)
    provider=models.TextField(max_length=5000, null=False, blank=True)

class GarageTemplate(models.Model):
    premium_gar=models.TextField(max_length=5000, null=False, blank=True)
    police_fe=models.TextField(max_length=5000, null=False, blank=True)
    inspection_fe=models.TextField(max_length=5000, null=False, blank=True)
    surplus=models.TextField(max_length=5000, null=False, blank=True)

class PackagTemplate(models.Model):
    package_premium=models.TextField(max_length=5000, null=False, blank=True)
    inspection_fe=models.TextField(max_length=5000, null=False, blank=True)
    police_fe=models.TextField(max_length=5000, null=False, blank=True)
    surplus=models.TextField(max_length=5000, null=False, blank=True)
    service_office=models.TextField(max_length=5000, null=False, blank=True)
    misc_state=models.TextField(max_length=5000, null=False, blank=True)


class File(models.Model):
    file_name=models.TextField(max_length=5000, null=False, blank=True)
    classification=models.TextField(max_length=5000, null=False, blank=True)
    status=models.TextField(max_length=5000, null=False, blank=True)
    datetime=models.TextField()



