from rest_framework import serializers
from rest_framework.serializers import Serializer, FileField
from api_get_data.models import LiabilityTemplate, PropertyTemplate, GarageTemplate, PackagTemplate, File

class LiabilityTemplateSerializers(serializers.ModelSerializer):
    class Meta:
        model = LiabilityTemplate
        exclude = ['is_removed'',created', 'modified']

class PropertyTemplateSerializers(serializers.ModelSerializer):
    class Meta:
        model = PropertyTemplate
        exclude = ['is_removed', 'created', 'modified']

class GarageTemplateSerializers(serializers.ModelSerializer):
    class Meta:
        model = GarageTemplate
        exclude = ['is_removed', 'created', 'modified']

class PackagTemplateSerializers(serializers.ModelSerializer):
    class Meta:
        model = PackagTemplate
        exclude = ['is_removed', 'created', 'modified']

# Serializers define the API representation.
class UploadSerializer(Serializer):
    file_uploaded = FileField()
    class Meta:
        fields = ['file_uploaded']

class UploadSerializer1(Serializer):
    file_uploaded = FileField()
    class Meta:
        fields = ['file_uploaded']

class FileSerializers(serializers.ModelSerializer):
    class Meta:
        model = File
        exclude = ['is_removed', 'created', 'modified']