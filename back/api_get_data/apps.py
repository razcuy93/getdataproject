from django.apps import AppConfig


class ApiGetDataConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api_get_data'
