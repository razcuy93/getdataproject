from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import GarageTemplate,LiabilityTemplate,PackagTemplate,PropertyTemplate
from .serializers import LiabilityTemplateSerializers, PackagTemplateSerializers, GarageTemplateSerializers,PropertyTemplateSerializers
from rest_framework import status
from exractPDFinfo.getData import Liability_Template, property_templeta, Package_Template, garage_template, File
from django.http import Http404
import PyPDF2
import re
import pdfquery
from lxml import etree
from django.http import JsonResponse
from django.views import View
import pandas
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from .serializers import UploadSerializer
from fpdf import FPDF
from django.core.files.storage import default_storage

# Create your views here.
class LiabilityTemplate_APIView(APIView):
    def get(self, request, format=None, *args, **kwargs):
        #Liability_Template(request.GET['text'])
        liability = LiabilityTemplate.objects.all().order_by("-id")[0:1]
        #lon = LiabilityTemplate.objects.count()-1
        #liability=liability[lon]
        #print(liability)
        #LiabilityTemplate.objects.all().delete()
        #serializer = LiabilityTemplateSerializers(liability, many=True)
        return JsonResponse(list(liability.values()), safe=False)

    def post(self, request, format=None):
        serializer = LiabilityTemplateSerializers(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PropertyTemplate_APIView(APIView):
    def get(self, request, format=None, *args, **kwargs):
        #property_templeta(request.GET['text'])
        property = PropertyTemplate.objects.all().order_by("-id")[0:1]

        #serializer = LiabilityTemplateSerializers(liability, many=True)
        return JsonResponse(list(property.values()), safe=False)


    def post(self, request, format=None):
        serializer = PropertyTemplateSerializers(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class GarageTemplate_APIView(APIView):
    def get(self, request, format=None, *args, **kwargs):
        #garage_template(request.GET['text'])
        garage = GarageTemplate.objects.all().order_by("-id")[0:1]

        #serializer = LiabilityTemplateSerializers(liability, many=True)
        return JsonResponse(list(garage.values()), safe=False)


    def post(self, request, format=None):
        serializer = GarageTemplateSerializers(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
class PackagTemplate_APIView(APIView):
    def get(self, request, format=None, *args, **kwargs):
        #Package_Template(request.GET['text'])
        package = PackagTemplate.objects.all().order_by("-id")[0:1]

        
        #serializer = LiabilityTemplateSerializers(liability, many=True)
        return JsonResponse(list(package.values()), safe=False)


    def post(self, request, format=None):
        serializer = PackagTemplateSerializers(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class File_APIView(APIView):
    def get(self, request, format=None, *args, **kwargs):
        file = File.objects.all()
        return JsonResponse(list(file.values()), safe=False)

    def post(self, request, format=None, *args, **kwargs):
        file = File.objects.all().delete()
        return JsonResponse(file, safe=False)

class UploadViewSetLeability(ViewSet):
    serializer_class = UploadSerializer

    def list(self, request):
        return Response("GET API")

    def create(self, request):
        file_uploaded = request.FILES.get('file_uploaded')
        print(request)
        #content_type = file_uploaded.content_type
        filename = "exractPDFinfo//"+str(file_uploaded)  # received file name
        file_obj = request.data['file_uploaded']
        with default_storage.open(filename, 'wb+') as destination:
            for chunk in file_obj.chunks():
                destination.write(chunk)
 
        Liability_Template(filename)


        file = File.objects.all()
        # serializer = LiabilityTemplateSerializers(liability, many=True)
        return JsonResponse(list(file.values()), safe=False)


class UploadViewSetPackage(ViewSet):
    serializer_class = UploadSerializer

    def list(self, request):
        return Response("GET API")

    def create(self, request):
        file_uploaded = request.FILES.get('file_uploaded')
        print(request)
        #content_type = file_uploaded.content_type
        filename = "exractPDFinfo//"+str(file_uploaded)  # received file name
        file_obj = request.data['file_uploaded']
        with default_storage.open(filename, 'wb+') as destination:
            for chunk in file_obj.chunks():
                destination.write(chunk)
 
        Package_Template(filename)

        file = File.objects.all()
        # serializer = LiabilityTemplateSerializers(liability, many=True)
        return JsonResponse(list(file.values()), safe=False)



class UploadViewSetGarage(ViewSet):
    serializer_class = UploadSerializer

    def list(self, request):
        return Response("GET API")

    def create(self, request):
        file_uploaded = request.FILES.get('file_uploaded')
        print(request)
        #content_type = file_uploaded.content_type
        filename = "exractPDFinfo//"+str(file_uploaded)  # received file name
        file_obj = request.data['file_uploaded']
        with default_storage.open(filename, 'wb+') as destination:
            for chunk in file_obj.chunks():
                destination.write(chunk)

        garage_template(filename)

        file = File.objects.all()
        # serializer = LiabilityTemplateSerializers(liability, many=True)
        return JsonResponse(list(file.values()), safe=False)


class UploadViewSetProperty(ViewSet):
    serializer_class = UploadSerializer

    def list(self, request):
        return Response("GET API")

    def create(self, request):
        file_uploaded = request.FILES.get('file_uploaded')
        print(request)
        #content_type = file_uploaded.content_type
        filename = "exractPDFinfo//"+str(file_uploaded)  # received file name
        file_obj = request.data['file_uploaded']
        with default_storage.open(filename, 'wb+') as destination:
            for chunk in file_obj.chunks():
                destination.write(chunk)
 

        property_templeta(filename)

        file = File.objects.all()
        # serializer = LiabilityTemplateSerializers(liability, many=True)
        return JsonResponse(list(file.values()), safe=False)
