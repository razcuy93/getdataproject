from django.urls import path
from .views import *

app_name = 'api_get_data'

urlpatterns = [
    path('liability', LiabilityTemplate_APIView.as_view(),name='liability'),
    path('garage', GarageTemplate_APIView.as_view(),name='garage'),
    path('package', PackagTemplate_APIView.as_view(),name='package'),
    path('property', PropertyTemplate_APIView.as_view(),name='property'),
    path('file', File_APIView.as_view(),name='file'),
]