
from django.contrib import admin
from .models import LiabilityTemplate, PackagTemplate, PropertyTemplate,GarageTemplate

admin.site.register(LiabilityTemplate)
admin.site.register(PackagTemplate)
admin.site.register(PropertyTemplate)
admin.site.register(GarageTemplate)


