import "bootstrap/dist/css/bootstrap.min.css";
import Bar from "./Components/Bar";
import Titulo from "./Components/TituloPrincipal";
import SelecFuncion from "./Components/SelecFuncion";
import { SelectContext } from "./context/SelectContext";
import { useState } from "react";

function App() {
  const [seleccionado, setseleccionado] = useState("");

  return (
    <SelectContext.Provider value={{ seleccionado, setseleccionado }}>
      <div className="App">
        <header className="App-header">
          <Bar />
        </header>

        <div>
          <Titulo />
        </div>

        <div className="">
          <SelecFuncion />
        </div>
      </div>
    </SelectContext.Provider>
  );
}

export default App;
