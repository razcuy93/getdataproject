import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";

function Bar() {
  return (
    <>
      <Navbar bg="primary" variant="dark">
        <Container>
          <Navbar.Brand href="#home">GetTemplateInfo</Navbar.Brand>
          <Nav className="me-auto"></Nav>
        </Container>
      </Navbar>

      <br />
    </>
  );
}

export default Bar;
