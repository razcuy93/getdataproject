import React from "react";
import Table from "react-bootstrap/Table";
import axios from "axios";
import { useEffect } from "react";

let baseURL = "http://127.0.0.1:8000/api_get_data/file";

const Tabla = (props) => {
  const { post } = props;

  useEffect(() => {
    axios.get(baseURL).then((json) => json);
  }, [post]);

  return (
    <>
      <div className="App ">
        <div className=" mt-2 d-inline-flex p-2" align="left">
          <div className="row">
            <div className="col-12">
              <Table striped bordered hover>
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">File name</th>
                    <th scope="col"> Classification</th>
                    <th scope="col">Status</th>
                    <th scope="col"> Datetime</th>
                  </tr>
                </thead>
                <tbody>
                  {post.map((item) => (
                    <tr key={item.id}>
                      <td>{item.file_name} </td>
                      <td>{item.classification}</td>
                      <td>{item.status}</td>
                      <td>{item.datetime}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </div>
        </div>

        <br />
      </div>
    </>
  );
};

export default Tabla;
