import React from "react";
import ReactHTMLTableToExcel from "react-html-table-to-excel";
import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";
import Table from "react-bootstrap/Table";

const baseURL1 = "http://localhost:8000/api_get_data/liability";
const baseURL2 = "http://localhost:8000/api_get_data/garage";
const baseURL3 = "http://localhost:8000/api_get_data/property";
const baseURL4 = "http://localhost:8000/api_get_data/package";

const ExportarExel = (props) => {
  const { tabla } = props;
  console.log(tabla);

  const [post1, setPost1] = useState([]);
  const [post2, setPost2] = useState([]);
  const [post3, setPost3] = useState([]);
  const [post4, setPost4] = useState([]);

  useEffect(() => {
    axios.get(baseURL1).then((response) => {
      setPost1(response.data);
    });
  }, []);

  React.useEffect(() => {
    axios.get(baseURL2).then((response) => {
      setPost2(response.data);
    });
  }, []);

  useEffect(() => {
    axios.get(baseURL3).then((response) => {
      setPost3(response.data);
    });
  }, []);

  useEffect(() => {
    axios.get(baseURL4).then((response) => {
      setPost4(response.data);
    });
  }, []);

  return (
    <>
      <div className="row mt-5">
        <div className="col-6 mt-3">
          <label className="">File processing status</label>
        </div>
        <div className="col-6">
          <ReactHTMLTableToExcel
            id="test-table-xls-button"
            className="download-table-xls-button btn btn-sm btn-primary mb-1 mt-3  col-9"
            table={tabla}
            filename="tablexls"
            sheet="tablexls"
            buttonText="Export information to XLS"
          />
        </div>
      </div>

      <>
        <Table hidden id="table-liability" striped bordered hover>
          <thead className="thead-dark">
            <tr>
              <th scope="col">liability_premium</th>
              <th scope="col"> policy_fee</th>
              <th scope="col">inspection_fee</th>
              <th scope="col"> surplus</th>
              <th scope="col"> service_off</th>
              <th scope="col"> fhcl</th>
              <th scope="col"> surplus</th>
            </tr>
          </thead>
          <tbody>
            {post1.map((item) => {
              return (
                <tr key={item.id}>
                  <td>{item.liability_premium} </td>
                  <td>{item.policy_fee}</td>
                  <td>{item.inspection_fee}</td>
                  <td>{item.surplus}</td>
                  <td>{item.service_off}</td>
                  <td>{item.fhcl}</td>
                  <td>{item.fl}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>

        <Table hidden id="table-Package" striped bordered hover>
          <thead className="thead-dark">
            <tr>
              <th scope="col">package_premium</th>
              <th scope="col"> inspection_fe</th>
              <th scope="col">police_fe</th>
              <th scope="col"> surplus</th>
              <th scope="col"> service_office</th>
              <th scope="col"> misc_state</th>
            </tr>
          </thead>
          <tbody>
            {post4.map((item) => {
              return (
                <tr key={item.id}>
                  <td>{item.package_premium} </td>
                  <td>{item.inspection_fe}</td>
                  <td>{item.police_fe}</td>
                  <td>{item.surplus}</td>
                  <td>{item.service_office}</td>
                  <td>{item.misc_state}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>

        <Table hidden id="table-Garage" striped bordered hover>
          <thead className="thead-dark">
            <tr>
              <th scope="col">premium_gar</th>
              <th scope="col"> police_fe</th>
              <th scope="col">inspection_fe</th>
              <th scope="col"> surplus</th>
            </tr>
          </thead>
          <tbody>
            {post2.map((item) => {
              return (
                <tr key={item.id}>
                  <td>{item.premium_gar} </td>
                  <td>{item.police_fe}</td>
                  <td>{item.inspection_fe}</td>
                  <td>{item.surplus}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>

        <Table hidden id="table-Property" striped bordered hover>
          <thead className="thead-dark">
            <tr>
              <th scope="col">premium_prop</th>
              <th scope="col"> police_fe</th>
              <th scope="col">inspection_fe</th>
              <th scope="col"> surplus</th>
              <th scope="col"> stamping</th>
              <th scope="col"> fl_empatf</th>
              <th scope="col"> provider</th>
            </tr>
          </thead>
          <tbody>
            {post3.map((item) => {
              return (
                <tr key={item.id}>
                  <td>{item.premium_prop} </td>
                  <td>{item.police_fe}</td>
                  <td>{item.inspection_fe}</td>
                  <td>{item.surplus}</td>
                  <td>{item.stamping}</td>
                  <td>{item.fl_empatf}</td>
                  <td>{item.provider}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </>
    </>
  );
};

export default ExportarExel;
