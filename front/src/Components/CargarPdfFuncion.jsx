import React, { Fragment } from "react";
import axios from "axios";
import Form from "react-bootstrap/Form";
import { useState } from "react";
import toast from "react-hot-toast";
import { Toaster } from "react-hot-toast";
import Container from "react-bootstrap/esm/Container";
import Row from "react-bootstrap/esm/Row";
import Col from "react-bootstrap/esm/Col";
import { useEffect } from "react";
import Table from "react-bootstrap/Table";


import ExportarExel from "./ExportarExel";

let baseURL = "http://127.0.0.1:8000/api_get_data/file";

const CargarPDFFuncion = (props) => {
  const { select } = props;

  const [state, setState] = useState(null);
  const [post, setPost] = useState([]);
  const [tabla, setTabla] = useState("");

  useEffect(() => {
    axios.get(baseURL).then((response) => {
      setPost(response.data);
    });
  }, []);

  const handledoc = (e) => {
    e.preventDefault();
    setState({
      selectedFile: e.target.files[0],
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios.get(baseURL).then((response) => {
      setPost(response.data);
    });

    console.log("posted ", state.selectedFile);
    let formData = new FormData();
    formData.append("file_uploaded", state.selectedFile);

    const secretKey = localStorage.getItem("key");

    let url1 = "http://127.0.0.1:8000/uploadLeability/";
    let url2 = "http://localhost:8000/uploadPackage/";
    let url3 = "http://localhost:8000/uploadGarage/";
    let url4 = "http://localhost:8000/uploadProperty/";

    //########################################  //########################################  //########################################

    try {
      if (select === "liability") {
        axios

          .post(url1, formData, {
            headers: {
              "Content-Type": "multipart/form-data",
              Authorization: `token ${secretKey}`,
            },
          })
          .then((resp) => {
            console.log(resp.status);
            // window.location.reload(false);
          });
        localStorage.setItem("estado", select);

        toast.success("Successfully Liabiliti!");
        setTabla("table-liability")
      }

      //########################################  //########################################  //########################################
      else if (select === "Property") {
        axios

          .post(url4, formData, {
            headers: {
              "Content-Type": "multipart/form-data",
              Authorization: `token ${secretKey}`,
            },
          })
          .then((resp) => {
            console.log(resp.status);
            // window.location.reload(false);
          });
        toast.success("Successfully Property!");
        setTabla("table-Property")
       
      }

      //########################################  //########################################  //########################################
      else if (select === "Garage") {
        axios

          .post(url3, formData, {
            headers: {
              "Content-Type": "multipart/form-data",
              Authorization: `token ${secretKey}`,
            },
          })
          .then((resp) => {
            console.log(resp.status);
            // window.location.reload(false);
          });
        toast.success("Successfully Garage!");
        setTabla("table-Garage")

      }

      //########################################  //########################################  //########################################
      else if (select === "Package") {
        axios

          .post(url2, formData, {
            headers: {
              "Content-Type": "multipart/form-data",
              Authorization: `token ${secretKey}`,
            },
          })
          .then((resp) => {
            console.log(resp.status);
            // window.location.reload(false);
          });
        toast.success("Successfully Package!");
        setTabla("table-Package")
      }

      //########################################  //########################################  //########################################
      else if (select === "None") {
        toast.error("Please select clasification");
        return;
      } else if (select === "") {
        toast.error("Please select clasification");
        return;
      }
    } catch (error) {
      console.error(error);
      return;
    }
  };

  return (
    <>
      <Fragment>
        <Container />
        <Row>
          <Col className="mt-5 pt-1">
            <Form.Group controlId="formFile" className="mb-3 col-12 inputf">
              <Form.Label>Upload file to process</Form.Label>
              <Form.Control
                accept=".pdf"
                type="file"
                className="inputf"
                display="bock"
                onChange={handledoc}
              />
            </Form.Group>
          </Col>
          <Col>
            <form className="mt-5 pt-2 ms-5 row">
              <button
                className="btn btn-xs btn-primary mt-4 pb-2 ps-5 ms-3 pe-5 inputf "
                type="submit"
                onClick={(e) => handleSubmit(e)}
              >
                Process file
              </button>
            </form>
          </Col>
        </Row>
      </Fragment>

      <ExportarExel tabla={tabla} />

      <div className="App ">
        <div className=" mt-2 d-inline-flex p-2" align="left">
          <div className="row">
            <div className="col-12">
              <Table striped bordered hover>
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">File name</th>
                    <th scope="col"> Classification</th>
                    <th scope="col">Status</th>
                    <th scope="col"> Datetime</th>
                  </tr>
                </thead>
                <tbody>
                  {post.map((item) => (
                    <tr key={item.id}>
                      <td>{item.file_name} </td>
                      <td>{item.classification}</td>
                      <td>{item.status}</td>
                      <td>{item.datetime}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </div>
        </div>

        <br />
      </div>

      <Toaster position="bottom-center" reverseOrder={false} />
    </>
  );
};

export default CargarPDFFuncion;
