import React, { useContext } from "react";
import Form from "react-bootstrap/Form";

import { useState } from "react";
import CargarPDFFuncion from "./CargarPdfFuncion";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { SelectContext } from "../context/SelectContext";



const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

const SelecFuncion = () => {

  const {seleccionado, setseleccionado}= useContext(SelectContext)

  const classes = useStyles();
  const [select, setSelect] = useState("");


  

  const onchange = (e) => {
    setseleccionado(e.target.value);
  };

  const onsubmitselect = (e) => {
    setseleccionado(e.target.value);
    console.log(seleccionado);
  };

  return (
    <div className="container">
      <div className={classes.root}>
        <Grid container spacing={2}>
          <Grid item xs={8}>
            <CargarPDFFuncion select={seleccionado} />
          </Grid>

          <Grid item xs className=" mb-5 mt-3">
            <div className="ms-5">
              <div className={classes.paper}>
                <div className={classes.paper}>
                  <form className="" onSubmit={onsubmitselect}>
                    <label>Classification</label>
                    <Form.Select
                      value={seleccionado}
                      className="mt-2 mb-5 "
                      onChange={onchange}
                      aria-label="Default select example"
                    >
                      <option value={"None"}>Select classification</option>
                      <option value="liability">Liabiliti</option>
                      <option value="Property">Property</option>
                      <option value="Garage">Garage</option>
                      <option value="Package">Package</option>
                    </Form.Select>
                  </form>
                </div>
              </div>
            </div>
          </Grid>
        </Grid>
      </div>
    
      
    </div>
  );
};

export default SelecFuncion;
